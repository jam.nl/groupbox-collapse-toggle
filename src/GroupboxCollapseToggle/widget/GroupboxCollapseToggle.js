define([
    "dojo/_base/declare",
    "mxui/widget/_WidgetBase",
], function (declare, _WidgetBase) {
    "use strict";

    return declare("GroupboxCollapseToggle.widget.GroupboxCollapseToggle", [_WidgetBase], {


        // Internal variables.
        _handles: null,
        _contextObj: null,

        constructor: function () {
            this._handles = [];
        },

        update: function (obj, callback) {
            this._contextObj = obj;
            this._updateRendering(callback);
        },

        _updateRendering: function (callback) {
            this.setOnClickEventForAllGroupboxes();
            this._executeCallback(callback, "_updateRendering");
        },

        setOnClickEventForAllGroupboxes: function () {
            var callback = function (mutationsList, observer) {
                for (var i = 0; i <= mutationsList.length; i++) {
                    var mutation = mutationsList[i];
                    if (mutation){
                        var elements = dojo.query(".groupboxCollapseToggle", mutation.target);
                        elements.forEach(function (groupboxCollapseToggle) {
                            dojo.disconnect(groupboxCollapseToggle.mendixOnClickHandle);
                            groupboxCollapseToggle.mendixOnClickHandle = dojo.connect(groupboxCollapseToggle, "onclick", function () {
                                    var groupboxes = dojo.query(groupboxCollapseToggle).siblings(".mx-groupbox.mx-groupbox-collapsible");
                                    groupboxes.forEach(function (groupboxElement) {
                                        dojo.query("h2.mx-groupbox-header", groupboxElement).forEach(function (headerElement) {
                                            if (headerElement.hasChildNodes()){
                                                headerElement.firstElementChild.click();
                                            }else{
                                                headerElement.click();
                                            }
                                        });
                                    });
                                }
                            );
                        });
                    }
                }
            };

            var config = {attributes: false, childList: true, subtree: true};
            var observer = new MutationObserver(callback);
            var contentForm = mx.ui.getContentForm();
            if (contentForm.mendixObserver) {
                contentForm.mendixObserver.disconnect();
            }
            contentForm.mendixObserver = observer;
            observer.observe(contentForm.domNode, config);
        },

        // Shorthand for executing a callback, adds logging to your inspector
        _executeCallback: function (cb, from) {
            if (cb && typeof cb === "function") {
                cb();
            }
        }
    });
});

require(["GroupboxCollapseToggle/widget/GroupboxCollapseToggle"]);